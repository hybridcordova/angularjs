import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SearchMovieComponent } from './search-movie/search-movie.component';
import {AddMovieComponent} from './add-movie/add-movie.component';


const routes : Routes =[
  { path: 'searchMovie', component: SearchMovieComponent },
  { path: 'addMovie', component: AddMovieComponent }
];;

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})

export class AppRoutingModule { }
