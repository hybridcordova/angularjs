export class Movie {
    name;
    rating;
    genre;
}

export const movies: Movie[] = [
    {
        name: 'Man of Steel',
        rating: 7.1,
        genre: 'Action'
    },
    {
        name: 'Batman v Superman: Dawn of Justice',
        rating: 6.6,
        genre: 'Fiction'
    },
    {
        name: 'Wonder Woman',
        rating: 7.6,
        genre: 'Action'
    },
    {
        name: 'Inception',
        rating: 7.1,
        genre: 'Fiction'
    },
    {
        name: 'Batman v Superman: Dawn of Justice',
        rating: 6.6,
        genre: 'Satire'
    },
    {
        name: 'Wonder Woman',
        rating: 7.6,
        genre: 'Drama'
    },
];

export const genres = ['Drama', 'Fiction', 'Satire', 'Action'];