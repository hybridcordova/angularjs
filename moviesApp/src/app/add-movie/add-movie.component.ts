import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MovieinfoService } from "../movieinfo.service";
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-add-movie',
  templateUrl: './add-movie.component.html',
  styleUrls: ['./add-movie.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class AddMovieComponent implements OnInit {

  nameFlag: boolean = false;
  ratingFlag: boolean = false;
  genreFlag: boolean = false;
  movieForm: any = FormGroup;
  genres: any = [];
<<<<<<< HEAD
  movie:any = {};
  check={};
=======
  //movie: any = {};
>>>>>>> 8e7d81e1fa872224c288f139a708f6e89b25abab

  constructor(public fb: FormBuilder, public MovieinfoService: MovieinfoService) {
    this.genres = this.MovieinfoService.genres;
<<<<<<< HEAD
    // this.movie = this.MovieinfoService.Movie;
  }

  createForm() {
    this.movieForm = this.fb.group({
      name: ['', Validators.required], // <--- the FormControl called "name"
      rating: [, Validators.required],
      genre: ['', Validators.required]
    });
=======
    //this.movie = this.MovieinfoService.Movie;
>>>>>>> 8e7d81e1fa872224c288f139a708f6e89b25abab
  }

  ngOnInit() {
    console.log(this.MovieinfoService.movies);

    this.movieForm = new FormGroup({
      'name': new FormControl('', [
        Validators.required,
        Validators.pattern('[a-z A-Z 0-9]+')
      ]),
      'rating': new FormControl('', [
        Validators.required,
        Validators.min(1),
        Validators.max(5)
      ]),
      'genre': new FormControl('', [
        Validators.required
      ])
    });
  }

  addMovie() {
<<<<<<< HEAD
    let movie:any ={};
     movie.name = this.movieForm.get('name').value;
     movie.rating = this.movieForm.get('rating').value;
     movie.genre = this.movieForm.get('genre').value;
     console.log(this.movie);
     console.log(this.MovieinfoService.movies);
     
     this.MovieinfoService.update(movie);
     console.log(this.MovieinfoService.movies);
=======
    this.genreFlag = false;
    this.ratingFlag = false;
    this.nameFlag = false;

    if (this.movieForm.controls['name'].hasError('required')){
      this.nameFlag = true;
    }

    if (this.movieForm.invalid && (this.movieForm.controls['rating'].hasError('required'))) {
      this.ratingFlag = true;
    }
    if (this.movieForm.controls['genre'].hasError('required')) {
      this.genreFlag = true;
    }
    if (this.genreFlag == false && this.ratingFlag == false && this.nameFlag == false) {
      let movie: any = {};
      movie.name = this.movieForm.get('name').value;
      movie.rating = this.movieForm.get('rating').value;
      movie.genre = this.movieForm.get('genre').value;
      //console.log(this.movie);
      this.MovieinfoService.movies.push(movie);
      console.log(this.MovieinfoService.movies);
    }
>>>>>>> 8e7d81e1fa872224c288f139a708f6e89b25abab
  }
}
