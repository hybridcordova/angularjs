import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {MovieinfoService} from '../movieinfo.service';

@Component({
  selector: 'app-search-movie',
  templateUrl: './search-movie.component.html',
  styleUrls: ['./search-movie.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class SearchMovieComponent implements OnInit {
 
  constructor(public movieSerive : MovieinfoService) { }

list=this.movieSerive.genres;
selectedGenre = this.list[0];
result:any=[];
  ngOnInit() {
    console.log(this.list);
  }
 
  onChange(obj){
    this.result= this.movieSerive.filterByGenre(this.selectedGenre);
  }

  

}