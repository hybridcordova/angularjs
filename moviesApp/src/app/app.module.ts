import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';


import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { SearchMovieComponent } from './search-movie/search-movie.component';
import { AddMovieComponent } from './add-movie/add-movie.component';
import {MovieinfoService} from './movieinfo.service';


@NgModule({
  declarations: [
    AppComponent,
    SearchMovieComponent,
    AddMovieComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [MovieinfoService],
  bootstrap: [AppComponent]
})
export class AppModule { }
